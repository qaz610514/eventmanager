using System;
using System.Collections.Generic;
using System.Linq;

public static class EventManager
{
    #region DataStruct
        
        private abstract class EventDataBase
        {
            
        }
        
        private class EventItem : EventDataBase 
        {
            public Enum Enum;

            public Action Action;
        }

        private class EventItem<T> : EventDataBase 
        {
            public Enum Enum;

            public Action<T> Action;
        }

        private class EventItem<T1, T2> : EventDataBase
        {
            public Enum Enum;

            public Action<T1, T2> Action;
        }
        
        #endregion

        private static readonly List<EventDataBase> TotalEvent = new List<EventDataBase>();

        public static void StartListening(Enum eventEnum, Action callBack) 
        {
            var items = TotalEvent.FindAll(eventItem => eventItem.GetType() == typeof(EventItem)).OfType<EventItem>().ToList();

            var events = items.FindAll(item => item.Enum.Equals(eventEnum)).FindAll(item => item.Action.Equals(callBack));

            if (!events.Any()) 
            {
                var item = new EventItem 
                {
                    Enum = eventEnum,
                    Action = callBack 
                };

                TotalEvent.Add(item);
            }
        }

        public static void StartListening<T>(Enum eventEnum, Action<T> callBack)
        {
            var items = TotalEvent.FindAll(eventItem => eventItem.GetType() == typeof(EventItem<T>)).OfType<EventItem<T>>().ToList();

            var events = items.FindAll(item => item.Enum.Equals(eventEnum)).FindAll(item => item.Action.Equals(callBack));

            if (!events.Any())
            {
                var item = new EventItem<T>
                {
                    Enum = eventEnum,
                    Action = callBack
                };

                TotalEvent.Add(item);
            }
        }

        public static void StartListening<T1, T2>(Enum eventEnum, Action<T1, T2> callBack)
        {
            var items = TotalEvent.FindAll(eventItem => eventItem.GetType() == typeof(EventItem<T1, T2>)).OfType<EventItem<T1, T2>>().ToList();

            var events = items.FindAll(item => item.Enum.Equals(eventEnum)).FindAll(item => item.Action.Equals(callBack));

            if (!events.Any())
            {
                var item = new EventItem<T1, T2>
                {
                    Enum = eventEnum,
                    Action = callBack
                };

                TotalEvent.Add(item);
            }
        }

        public static void StopListening(Enum eventEnum, Action callBack)
        {
            var items = TotalEvent.FindAll(eventItem => eventItem.GetType() == typeof(EventItem)).OfType<EventItem>().ToList();

            var events = items.FindAll(item => item.Enum.Equals(eventEnum)).FindAll(item => item.Action.Equals(callBack));

            if (events.Any())
            {
               foreach(var item in events) 
               {
                    TotalEvent.Remove(item); 
               }
            }
        }

        public static void StopListening<T>(Enum eventEnum, Action<T> callBack)
        {
            var items = TotalEvent.FindAll(eventItem => eventItem.GetType() == typeof(EventItem<T>)).OfType<EventItem<T>>().ToList();

            var events = items.FindAll(item => item.Enum.Equals(eventEnum)).FindAll(item => item.Action.Equals(callBack));

            if (events.Any())
            {
                foreach (var item in events)
                {
                    TotalEvent.Remove(item);
                }
            }
        }

        public static void StopListening<T1, T2>(Enum eventEnum, Action<T1, T2> callBack)
        {
            var items = TotalEvent.FindAll(eventItem => eventItem.GetType() == typeof(EventItem<T1, T2>)).OfType<EventItem<T1, T2>>().ToList();

            var events = items.FindAll(item => item.Enum.Equals(eventEnum)).FindAll(item => item.Action.Equals(callBack));

            if (events.Any())
            {
                foreach (var item in events)
                {
                    TotalEvent.Remove(item);
                }
            }
        }

        public static void EmitEvent(Enum eventEnum)
        {
            var items = TotalEvent.FindAll(eventItem => eventItem.GetType() == typeof(EventItem)).OfType<EventItem>().ToList();

            var events = items.FindAll(item => item.Enum.Equals(eventEnum));

            foreach(var item in events) 
            {
                item.Action?.Invoke(); 
            }
        }

        public static void EmitEvent<T>(Enum eventEnum, T value)
        {
            var items = TotalEvent.FindAll(eventItem => eventItem.GetType() == typeof(EventItem<T>)).OfType<EventItem<T>>().ToList();

            var events = items.FindAll(item => item.Enum.Equals(eventEnum));

            foreach (var item in events)
            {
                item.Action?.Invoke(value);
            }
        }

        public static void EmitEvent<T1, T2>(Enum eventEnum, T1 value1, T2 value2)
        {
            var items = TotalEvent.FindAll(eventItem => eventItem.GetType() == typeof(EventItem<T1, T2>)).OfType<EventItem<T1, T2>>().ToList();

            var events = items.FindAll(item => item.Enum.Equals(eventEnum));

            foreach (var item in events)
            {
                item.Action?.Invoke(value1, value2);
            }
        }

        public static void RemoveAllListening()
        {
            TotalEvent.Clear();
        }
}
